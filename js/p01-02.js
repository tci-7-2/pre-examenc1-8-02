// Función para cargar las razas de perros sin mostrar imágenes
async function cargarRazas() {
    try {
        const response = await fetch('https://dog.ceo/api/breeds/list');
        const data = await response.json();
        const breeds = data.message;

        // Obtener el elemento select
        const select = document.getElementById('breedsSelect');
        select.innerHTML = ""; // Limpiar el select

        // Llenar el select con las razas de perros
        breeds.forEach(breed => {
            const option = document.createElement('option');
            option.value = breed;
            option.textContent = breed;
            select.appendChild(option);
        });
    } catch (error) {
        console.error('Error al cargar las razas de perros:', error);
    }
}

// Función para obtener y mostrar la imagen del perro seleccionado
async function verImagen() {
    const selectedBreed = document.getElementById('breedsSelect').value;
    const dogImageElement = document.getElementById('dogImage');

    if (selectedBreed) {
        try {
            // Obtener la imagen del perro de la API
            const response = await fetch(`https://dog.ceo/api/breed/${selectedBreed}/images/random`);
            const data = await response.json();
            const imageUrl = data.message;

            // Mostrar la imagen del perro
            dogImageElement.src = imageUrl;
        } catch (error) {
            console.error('Error al obtener la imagen del perro:', error);
        }
    } else {
        // Si no se selecciona ninguna raza, mostrar un mensaje de error
        console.error('Por favor, selecciona una raza de perro.');
    }
}

// Asociar la función cargarRazas al evento click del botón "Cargar Razas"
document.getElementById('cargarRaza').addEventListener('click', cargarRazas);

// Asociar la función verImagen al evento click del botón "Ver Imagen"
document.getElementById('verImagen').addEventListener('click', verImagen);