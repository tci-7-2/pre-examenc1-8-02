const input = document.getElementById('buscar');
const button = document.getElementById('btnBuscar');
// Llamado a FETCH //
// Usando Promesas // 
const llamandoFetch = () => {
    const name = input.value.trim(); // Elimina espacios en blanco al inicio y al final
    if (name === "") {
        mostrarError("Por favor ingrese un nombre de país.");
        return;
    }
    const url = `https://restcountries.com/v3.1/name/${name}`;
    fetch(url)
        .then(respuesta => {
            if (!respuesta.ok) {
                throw new Error('Error al obtener los datos del país.');
            }
            return respuesta.json();
        })
        .then(data => {
            if (data.status === 404 || data.length === 0) {
                throw new Error('País no encontrado.');
            }
            mostrarTodos(data);
        })
        .catch(error => {
            mostrarError(error.message);
        });
}

const mostrarTodos = (data) => {
    const res = document.getElementById('respuesta');
    res.innerHTML = "";

    data.forEach(item => {
        res.innerHTML += `Capital: ${item.capital}<br>`;
        res.innerHTML += "Lenguajes: <ul>";
        for (let langCode in item.languages) {
            res.innerHTML += `<li>${item.languages[langCode]} (${langCode})</li>`;
        }
        res.innerHTML += "</ul><br>";
    });
}

const mostrarError = (mensaje) => {
    const res = document.getElementById('respuesta');
    res.innerHTML = `<p style="color: red;">${mensaje}</p>`;
}

document.getElementById("btnBuscar").addEventListener('click', llamandoFetch);

document.getElementById("btnLimpiar").addEventListener('click', () => {
    const respuesta = document.getElementById("respuesta");
    respuesta.innerHTML = "";
});
