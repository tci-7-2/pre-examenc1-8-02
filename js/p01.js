document.getElementById("btnBuscar").addEventListener("click", buscarVendedor);
document.getElementById("btnLimpiar").addEventListener("click", limpiarCampos);

function buscarVendedor() {
    const userId = document.getElementById("userId").value.trim();

    if (!userId) {
        alert("Por favor ingrese un ID de vendedor válido.");
        return;
    }

    const url = `https://jsonplaceholder.typicode.com/users/${userId}`;

    axios.get(url)
        .then(response => {
            mostrarInformacion(response.data);
        })
        .catch(error => {
            alert("No se encontró ningún vendedor con ese ID.");
            console.error("Error al obtener información del vendedor:", error);
        });
}

function mostrarInformacion(vendedor) {
    document.getElementById("txtNombre").value = vendedor.name;
    document.getElementById("txtNomUser").value = vendedor.username;
    document.getElementById("txtEmail").value = vendedor.email;
    document.getElementById("txtCalle").value = vendedor.address.street;
    document.getElementById("txtNumero").value = vendedor.address.suite;
    document.getElementById("txtCiudad").value = vendedor.address.city;
}

function limpiarCampos() {
    document.getElementById("userId").value = "";
    document.getElementById("txtNombre").value = "";
    document.getElementById("txtNomUser").value = "";
    document.getElementById("txtEmail").value = "";
    document.getElementById("txtCalle").value = "";
    document.getElementById("txtNumero").value = "";
    document.getElementById("txtCiudad").value = "";
}
